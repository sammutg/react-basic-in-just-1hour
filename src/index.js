import React from 'react';
import ReactDOM from 'react-dom';
import { Component } from 'react';

class Product extends Component {

    getInitialState() {
        return { qty: 0 };
    }

    buy() {
        // alert("You Buy a Android Mobile")
        this.setState({ qty: this.state.qty + 1 });
    }

    render() {
        return ( 
            <div>
                <p>Android: $199</p> 
                <button onClick={this.buy}>Buy</button>
                <p>Qty: {this.state.qty} items</p>
            </div>
        );
    }
};

ReactDOM.render(<Product />, document.getElementById('root'));